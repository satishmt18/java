class LocalVariable3 {
	public static void main(String args[]) {
		int b = 10;		
		{
			int b = 20;
			System.out.println("Value of b inside = " + b);
		}
		System.out.println("Value of b outside = " + b);
	}
}