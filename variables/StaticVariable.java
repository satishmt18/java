class StaticVariable {
	static int intVar;
	static float floatVar;
	static Object obj;
	static boolean boolVar;

	public static void main(String args[]) {
		System.out.println("Default value of intVar = " + intVar);
		System.out.println("Default value of floatVar = " + floatVar);
		System.out.println("Default value of Object = " + obj);
		System.out.println("Default value of boolVar = " + boolVar);
	}
}