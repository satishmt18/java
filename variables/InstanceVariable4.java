/* Instance variable Values can be assigned during 
the declaration or within the constructor
*/
class InstanceVariable4 {
	float a = 10.5f;
	int myVar;
	InstanceVariable4() { 		// default constructor
		myVar = 10;
	}	
	public void test() {
		int b = 10;				// local variable
		System.out.println("Value of a = " + a);
		System.out.println("Value of b = " + b);
		System.out.println("Value of myVar = " + myVar);
	}
	public static void main(String args[]) {
		InstanceVariable4 iv = new InstanceVariable4();
		iv.test();
	}
}