/* Instance variables hold values 
that can be referenced by more than one method
*/
class InstanceVariable3 {
	
	public void test() {
		int b = 10;				// local variable
		System.out.println("Value of a = " + a);
		System.out.println("Value of b = " + b);
	}

	int a;		// instance variable

	public void test2() {
		System.out.println("Value of a = " + a);
	}

	public static void main(String args[]) {
		InstanceVariable3 iv = new InstanceVariable3();
		iv.test();
		iv.test2();
	}
}