class LocalVariable2 {
	public static void main(String args[]) {
		int a = 10;		
		{
			int b = 10;
			System.out.println("Value of a = " + a);
		}
		System.out.println("Value of b = " + b);	//compile time error
	}
}