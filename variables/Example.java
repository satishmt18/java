class Example {
	public static void main(String args[]) {
		int firstVariable; 		// Variable declaration
		firstVariable = 10;		// Variable assignment
		// int firstVariable = 10; //both in single statement
		System.out.println("First variable = " + firstVariable);
	}
}