class InstanceClass {
	int rollNo;			// instance variable
	String name;
    String dept;
    InstanceClass(int id, String a, String b) {
    	rollNo = id;
    	name = a;
    	dept = b;
    }
    void display() {
    	System.out.println(" Roll No: " + rollNo + " " + "name: " 
    		+ name + " dept:" + dept);
    }
}

class InstanceVariable5 {
	public static void main(String args[]) {
		InstanceClass ic = new InstanceClass(1, "Ravi", "CSE");
		ic.display();
	}
}