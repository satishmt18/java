class LocalVariable {
	public static void main(String args[]) {
		int a;		// variable is not initialized
		System.out.println("Value of a = " + a); //compile time error
	}
}