class Student {
	int rollNo;					// instance variable
	String name;
	static String college = "Raudra";	//static variable
	Student(int r, String n) {
		rollNo = r;	
		name = n;
	}
	void display() {
		System.out.println(rollNo + " " + name + " " + college);
	}
}
class StudentMain {
	public static void main(String args[]) {
		Student s1 = new Student(1, "ravi");
		Student s2 = new Student(2, "babu");
		s1.display();
		s2.display();
	}
}