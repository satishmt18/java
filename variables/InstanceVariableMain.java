class InstanceVariable {

	int intField;
	float floatField;
	double doubleField;
	Object ob;
}

class InstanceVariableMain {

	public static void main(String args[]) {
		InstanceVariable iv = new InstanceVariable();
		System.out.println("Default value of int = " + iv.intField);
		System.out.println("Default value of float = " + iv.floatField);
		System.out.println("Default value of double = " + iv.doubleField);
		System.out.println("Default value of object = " + iv.ob);
	}
}