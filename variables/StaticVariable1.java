class StaticVariable1 {
	static int intVar;
	static double doubleVar = 10.5;
	static {
		intVar = 10;
	}
	public static void main(String args[]) {
		System.out.println("value of intVar = " + intVar);
		System.out.println("value of doubleVar = " + doubleVar);
	}
}