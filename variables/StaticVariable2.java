class StaticVariable2 {

	static int count = 0;
	public void increment() {
		count++;
	}
}

class StaticVariableMain {
	public static void main(String args[]) {
		StaticVariable2 sv1 = new StaticVariable2();
		StaticVariable2 sv2 = new StaticVariable2();
		sv1.increment();
		sv2.increment();
		System.out.println("Object1: count = "+ sv1.count);
		System.out.println("Object2: count = "+ sv2.count);
	}
}