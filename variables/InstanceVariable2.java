/* Instance variables can be declared in a body of a class 
 before or after use.
*/
class InstanceVariable2 {
	public void test() {
		int b = 10;				// local variable
		System.out.println("Value of a = " + a);
		System.out.println("Value of b = " + b);
	}
	int a;		// instance variable
	public static void main(String args[]) {
		InstanceVariable2 iv = new InstanceVariable2();
		iv.test();
	}
}