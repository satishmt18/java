class StaticFinalVariable {
	public static final double PI = 3.14;
	public static void main(String args[]) {
		StaticFinalVariable sv = new StaticFinalVariable();
		sv.PI = 3.145;		// compile time error, can't assign a value
							// to final variable
	}	
}