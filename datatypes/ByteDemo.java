class ByteDemo {
	public static void main(String args[]) {
		byte a = 10;
		System.out.println("Value of a = " + a);
		System.out.println("Size of Byte = " + Byte.SIZE + " " + "bits");
	}
}