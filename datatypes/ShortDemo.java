class ShortDemo {
	public static void main(String args[]) {
		short a = 10;
		System.out.println("Value of a = " + a);
		System.out.println("Size of Short = " + Short.SIZE + " " + "bits");
	}
}