class Test {
	public static void main(String args[]) {
		int i = 100;
		long l = i; // no explicit type casting required
		float f = l; // no explicit type casting required
		System.out.println("Value of i = " + i);
		System.out.println("Value of l = " + l);
		System.out.println("Value of f = " + f);
	}
}