class DoubleDemo {
	public static void main(String args[]) {
		double a = 10.5;
		System.out.println("Value of a = " + a);
		System.out.println("\r" + "Size of Double = " + Double.SIZE + " " 
			+ "\f" + "bits" + "\r");
	}
}