class FloatDemo {
	public static void main(String args[]) {
		float a = 10.5f;
		System.out.println("Value of a = " + a);
		System.out.println("Size of Float = " + Float.SIZE +
		 " " + "bits");
	}
}