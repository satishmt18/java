class LongDemo {
	public static void main(String args[]) {
		long a = 100000;
		System.out.println("Value of a = " + a);
		System.out.println("Size of long = " + Long.SIZE + " " + "bits");
	}
}