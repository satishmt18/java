class IntDemo {
	public static void main(String args[]) {
		int a = 10; 
		System.out.println("Value of a = " + a);
		System.out.println("Size of int is: " + Integer.SIZE + " bits");
	}
}