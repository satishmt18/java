class DataTypeCasting {
	public static void main(String args[]) {
		byte b;
		int i = 81;
		double d = 323.142;
		float f = 72.38f;
		char c = 'A';
		c = (char) i;	// Explicit convertion int to char
		System.out.println("i = " + i + " c = " + c);

		i = (int) d;	// Explicit convertion double to int
		System.out.println("d = " + d + " i = " + i);

		i = (int) f;	// Explicit convertion float to int
		System.out.println("f = " + f + " i = " + i);

		b = (byte) d;	// Explicit convertion double to byte
		System.out.println("d = " + d + " b = " + b);

		d = i;	// Implicit (Automatic) convertion int to double
		System.out.println("i = " + i + " d = " + d);
	}
}