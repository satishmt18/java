class IntToByte {
	public static void main(String args[]) {
		int a = 350;
		byte b = (byte) a; // Explicit Type casting
		System.out.println("Value of a = " + a);
		System.out.println("Value of b = " + b);
		System.out.println(a.getClass().getName());
	}
}