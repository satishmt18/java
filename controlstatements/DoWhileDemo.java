class DoWhileDemo {
	public static void main(String args[]) {
		int a = 0;
		do {
			System.out.println("a value = " + a);
			a++;
		}while(a <= 10);

		/* do {
			System.out.println("a value = " + a);
		}while(++a <= 10);*/
	}
}