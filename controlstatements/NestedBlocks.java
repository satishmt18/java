class NestedBlocks {
	public static void main(String args[]) {
		int a = 10, b = 20;
		System.out.println("a = " + a);
		{
			int c = a * 2;
			System.out.println("c in inner block 1 = " + c);
			a = 30;
			System.out.println("a in inner block 1 = " + a);
		}
		// System.out.println("c = " + c); won't work
		int d = 40;
		{
			int c = a * 3;
			System.out.println("c in inner block 2 = " + c);
			d = c;
			System.out.println("d in inner block 2 = " + d);
		}
		// int a = 50;  won't work
		System.out.println("a after blocks = " + a);
		System.out.println("d after blocks = " + d);
	}
}