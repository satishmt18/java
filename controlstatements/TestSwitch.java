class TestSwitch {
	public static void main(String args[]) {
		char letter = 'C';
		switch(letter)
		{
			case 'A':
		    case 'E': System.out.println("This is an vowel.");
		    case 'B':
		    case 'C':
		    case 'D': System.out.println("This is an alphabet.");
		    		  break;
		    case '@':
		    case '#': System.out.println("This is a special character.");
		        	  break;
		}
	}
}