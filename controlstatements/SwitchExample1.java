class SwitchExample1 {
	public static void main(String args[]) {
		char direction = 'E';
		switch (direction)
		{
			case 'E': System.out.println("East");
			case 'W': System.out.println("West");
			case 'S': System.out.println("South");
			case 'N': System.out.println("North");
					  break;
			default : System.out.println("Unknown direction");
					  break;
		}
	}
}