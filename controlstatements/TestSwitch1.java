class TestSwitch1 {
	public static void main(String args[]) {
		char direction = 'N';
		switch( direction )
		{
		    case 'E': System.out.println("East");
		        	  break;
		    case 'W': System.out.println("West");
		        	  break;
		    case 'S': System.out.println("South");
		        	  break;
		    case 'N': System.out.println("North");
		        	  break;
		    default : System.out.println("Unknown direction");
		        	  break;
		}
	}
}