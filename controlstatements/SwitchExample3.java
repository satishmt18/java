class SwitchExample3 {
	public static void main(String args[]) {
		char direction = 'A';
		switch (direction)
		{
			case 69: System.out.println("East");
					  break;
			case 87: System.out.println("West");
					  break;
			case 83: System.out.println("South");
					  break;
			case 78: System.out.println("North");
					  break;
			default : System.out.println("Unknown direction");
					  break;
		}
	}
}