class TestIf1 {
	public static void main(String args[]) {
		char direction = 'N';
		if( direction == 'E' )
		    System.out.println("East");
		else if( direction == 'W' )
		    System.out.println("West");
		else if( direction == 'S' )
		    System.out.println("South");
		else if( direction == 'N' )
		    System.out.println("North");
		else
		    System.out.println("Unknown direction");
	}
}