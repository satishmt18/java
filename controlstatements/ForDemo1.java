class ForDemo1 {
	public static void main(String args[]) {
		for(int i = 10; i > 0; i++)
			System.out.println("Value of i = " + i);
	}
}

// infinite loop the condition would never return false


// another example of infinite loop

/* 	for(; ; ) {
		statement (s)
	}
*/