class WhileDemo {
	public static void main(String args[]) {
		int n = 0;
		int sum = 0;
		while(n <= 10)
			sum += n++;
		System.out.println("Sum = " + sum);

	}
}
