class Assignment {
	public static void main(String args[]) {
		int a = 10;
		int b = 20;
		int c = 0;

		c = a + b;			//	Assignment operator
		System.out.println("c = a + b is " + c);

		c += a;				// 	Addition Assignment
		System.out.println("c += a is " + c);

		c -= a;				//	Subtraction Assignment
		System.out.println("c -= a is " + c);

		c *= a;				//	Multiplication Assignment
		System.out.println("c *= a is " + c);

		c /= a;				// 	Division Assignment
		System.out.println("c /= a is " + c);

		c %= a;				// Modulus Assignment
		System.out.println("c %= a is " + c);

		c <<= 2;			// Left shift Assignment
		System.out.println("c <<= 2 is " + c);

		c >>= 2;			// Right Shift Assignment
		System.out.println("c >>= 2 is " + c);

		c &= a;				// Bitwise AND assignment
		System.out.println("c &= a is " + c);

		c ^= a;				// Bitwise Exclusive OR Assignment
		System.out.println("c ^= a is " + c);

		c |= a;				// Bitwise OR Assignment
		System.out.println("c |= a is " + c);

	}
}