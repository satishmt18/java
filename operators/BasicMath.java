class BasicMath {
	public static void main(String args[]) {
		//arithmetic using Integers
		System.out.println("Integer Arithmetic");
		int a = 1 + 1;	// Addition
		int b = a * 3;	// Muliplication
		int c = b / 4;	// Division
		int d = c - a;	// Subtraction
		int e = b % a;	// Modulus
		System.out.println("a = " + a);
		System.out.println("b = " + b);
		System.out.println("c = " + c);
		System.out.println("d = " + d);
		System.out.println("e = " + e);
		//arithmetic Using doubles
		System.out.println("Floating Point Arithmetic");
		double da = 1 + 1;	
		double db = da * 3;
		double dc = db / 4;
		double dd = dc - da;
		double de = db % da;
		System.out.println("da = " + da);
		System.out.println("db = " + db);
		System.out.println("dc = " + dc);
		System.out.println("dd = " + dd);
		System.out.println("de = " + de);
	}
}