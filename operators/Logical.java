class Logical {
	public static void main(String args[]) {
		boolean a = true;
		boolean b = false;
		System.out.println("a && b is " + (a && b));
		System.out.println("a || b is " + (a || b));
		System.out.println("!(a && b) is " + !(a && b));
		int p = 10;
		int q = 20;
		int r = 30;
		System.out.println("(p < q)&&(q < r) is " +((p < q) && (q < r)));
		System.out.println("(p > q)&&(q < r) is " +((p > q) && (q < r)));	
	}
}