class IncDec1 {
	public static void main(String args[]) {
		int a, b, c, d;
		a = 1; 
		b = 2;
		c = --b;	//	Pre decrement
		d = a--;	// 	Post decrement
		c--;
		System.out.println("a = " + a);
		System.out.println("b = " + b);
		System.out.println("c = " + c);
		System.out.println("d = " + d);
	}
}